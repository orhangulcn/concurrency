package com.orhangulcan.concurrencyexample.data

import java.util.*

object MockResponse {
    fun getDataList(): List<ExampleData> {
        val dataList = mutableListOf<ExampleData>()

        (0..20).forEach {
            dataList.add(ExampleData("$it ${UUID.randomUUID()}"))
        }

        return dataList.toList()
    }
}
