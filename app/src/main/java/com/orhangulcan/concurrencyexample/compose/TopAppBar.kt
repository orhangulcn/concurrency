package com.orhangulcan.concurrencyexample.compose

import androidx.compose.foundation.layout.RowScope
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.heading
import androidx.compose.ui.semantics.semantics
import androidx.navigation.NavController
import com.orhangulcan.concurrencyexample.util.hasBottomNav

@Composable
fun WellTopAppBar(
    title: String,
    actions: @Composable RowScope.() -> Unit = {},
    navController: NavController,
    navigationIconVector: ImageVector = Icons.Filled.ArrowBack
) {
    WellTopAppBar(
        content = {
            Text(
                text = title,
                modifier = Modifier.semantics { heading() }
            )
        },
        actions = actions,
        navController = navController,
        navigationIconVector = navigationIconVector
    )
}

@Composable
fun WellTopAppBar(
    content: @Composable () -> Unit,
    actions: @Composable RowScope.() -> Unit = {},
    navController: NavController,
    navigationIconVector: ImageVector = Icons.Filled.ArrowBack
) {
    var navigationIcon: @Composable (() -> Unit)? = {
        WellIconButton(onClick = { navController.popBackStack() }) {
            Icon(
                imageVector = navigationIconVector,
                contentDescription = "Back Button",
            )
        }
    }
    navController.addOnDestinationChangedListener { _, destination, _ ->
        if (navController.currentBackStackEntry == null || destination.hasBottomNav(navController)
        ) {
            navigationIcon = null
        }
    }

    TopAppBar(
        backgroundColor = MaterialTheme.colors.surface,
        title = content,
        navigationIcon = navigationIcon,
        actions = actions
    )
}

@Composable
fun WellIconButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    content: @Composable () -> Unit
) {
    IconButton(
        onClick = onClick,
        modifier = modifier,
        enabled = enabled,
        content = content
    )
}

