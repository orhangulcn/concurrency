package com.orhangulcan.concurrencyexample.ui.one

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.Card
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.orhangulcan.concurrencyexample.R
import com.orhangulcan.concurrencyexample.compose.WellTopAppBar
import com.orhangulcan.concurrencyexample.data.ExampleData
import com.orhangulcan.concurrencyexample.theme.ConcurrencyTheme
import com.orhangulcan.concurrencyexample.util.UiState
import com.orhangulcan.concurrencyexample.util.UiStateContent
import kotlinx.coroutines.launch

class FragmentOne : Fragment() {

    private val viewModel: OneViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewModel.verifyNextScreen()
        Log.e("FragmentOne", "onCreateView")

        return ComposeView(requireContext()).apply {
            setContent {
                HomeRoute(
                    viewModel = viewModel,
                    navController = findNavController()
                )
            }
        }
    }

    override fun onPause() {
        super.onPause()
        Log.e("FragmentOne", "paused")
    }

    override fun onResume() {
        super.onResume()
        Log.e("FragmentOne", "resumed")
    }
}

@Composable
fun HomeRoute(
    viewModel: OneViewModel,
    navController: NavController
) {
    val nextScreen = viewModel.homeState.nextScreen
    LaunchedEffect(key1 = nextScreen) {
        when (nextScreen) {
            is HomeNextScreen.SecondScreen -> {
                navController.navigate(R.id.action_fragmentOne_to_fragmentTwo)
            }
            else -> {}
        }
        viewModel.getSuspendedDataList()
    }

    FirstScreen(viewModel.homeState.uiState, navController)
}

@Composable
fun FirstScreen(
    uiState: UiState<List<ExampleData>>,
    navController: NavController,
) {
    ConcurrencyTheme {
        Scaffold(
            topBar = {
                WellTopAppBar(
                    title = "First Screen",
                    navController = navController
                )
            }
        ) {
            UiStateContent(
                uiState = uiState,
                successContent = { homeContent ->
                    FirstListView(homeContent)
                }
            )
        }
    }
}

@Composable
fun FirstListView(
    dataList: List<ExampleData>,
) {
    LazyColumn(
        state = rememberLazyListState(),
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(16.dp),
        contentPadding = PaddingValues(horizontal = 24.dp, vertical = 24.dp),
    ) {
        items(dataList) { data ->
            Card(modifier = Modifier.padding(8.dp)) {
                Text(text = data.name, modifier = Modifier.fillMaxSize().padding(20.dp), textAlign = TextAlign.Center)
            }
        }
    }
}
