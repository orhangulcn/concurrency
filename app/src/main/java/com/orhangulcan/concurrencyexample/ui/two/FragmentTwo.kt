package com.orhangulcan.concurrencyexample.ui.two

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.orhangulcan.concurrencyexample.compose.WellTopAppBar
import com.orhangulcan.concurrencyexample.theme.ConcurrencyTheme

class FragmentTwo : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val navController = findNavController()
        return ComposeView(requireContext()).apply {
            setContent {
                ConcurrencyTheme {
                    SecondScreen(navController = navController) {
                        navController.popBackStack()
                    }
                }
            }
        }
    }
}

@Composable
fun SecondScreen(
    navController: NavController,
    pressEvent: () -> Unit
) {
    ConcurrencyTheme {
        Scaffold(
            topBar = {
                WellTopAppBar(
                    title = "Second Screen",
                    navController = navController
                )
            }
        ) {
            Text("SECOND SCREEN")
            Button(onClick = { pressEvent() }, modifier = Modifier.fillMaxSize()) {
                Text("Close", style = MaterialTheme.typography.h1)
            }
        }
    }
}
