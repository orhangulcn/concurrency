package com.orhangulcan.concurrencyexample.ui.one

import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.orhangulcan.concurrencyexample.data.ExampleData
import com.orhangulcan.concurrencyexample.data.MockResponse
import com.orhangulcan.concurrencyexample.util.UiState
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex

class OneViewModel : ViewModel() {
    var homeState by mutableStateOf(HomeState(UiState.Loading))
        private set

    private val mutex = Mutex()

    fun getDataList() {
        viewModelScope.launch {
            homeState = homeState.copy(uiState = UiState.Loading)
            delay(2500)
            homeState = homeState.copy(
                uiState = UiState.Content(MockResponse.getDataList())
            )
        }
    }

    suspend fun getSuspendedDataList() {
        homeState = homeState.copy(uiState = UiState.Loading)

        val response = MockResponse.getDataList()

        delay(2500)

        synchronized(homeState) {
            homeState = homeState.copy(
                uiState = UiState.Content(response),
                nextScreen = homeState.nextScreen
            )
        }

        /*mutex.withLock(homeState) {
            homeState = homeState.copy(
                uiState = UiState.Content(response)
            )
        }*/
    }

    fun verifyNextScreen() {
        Log.e("verifyNextScreen", "join")
        viewModelScope.launch {
            Handler(Looper.getMainLooper()).postDelayed({
                homeState = homeState.copy(nextScreen = HomeNextScreen.SecondScreen)
            }, 250L)
        }
    }

    override fun onCleared() {
        super.onCleared()
        Log.e("FragmentOneVM", "cleared")
    }
}

data class HomeState(
    val uiState: UiState<List<ExampleData>>,
    val nextScreen: HomeNextScreen? = null
)

sealed class HomeNextScreen {
    object SecondScreen : HomeNextScreen()
}
