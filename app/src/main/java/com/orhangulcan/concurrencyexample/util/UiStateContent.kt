package com.orhangulcan.concurrencyexample.util

import androidx.compose.material.Text
import androidx.compose.runtime.Composable

@Composable
fun <T> UiStateContent(
    uiState: UiState<T>,
    loadingContent: @Composable () -> Unit = { FullScreenLoading() },
    failureContent: @Composable (String?) -> Unit = { errorText ->
        errorText?.let { Text(text = it) }
    },
    successContent: @Composable (T) -> Unit
) {

    when (uiState) {
        is UiState.Loading -> {
            loadingContent()
        }
        is UiState.Error -> {
            failureContent(uiState.error)
        }
        is UiState.Content -> {
            if (uiState.data == null) {
                failureContent("Error")
            } else {
                successContent(uiState.data)
            }
        }
    }
}
