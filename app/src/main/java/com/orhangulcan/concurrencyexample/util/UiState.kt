package com.orhangulcan.concurrencyexample.util

sealed class UiState<out R> {
    object Loading : UiState<Nothing>()
    data class Content<out T>(val data: T?) : UiState<T>()
    data class Error(
        val error: String?
    ) : UiState<Nothing>()
}

val <T : Any> UiState<T>.requireContent
    get() = (this as UiState.Content).data!!

