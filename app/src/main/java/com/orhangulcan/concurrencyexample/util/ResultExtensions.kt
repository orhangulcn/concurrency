package com.orhangulcan.concurrencyexample.util

fun <T> Result<T>.getFailureValue() = this.exceptionOrNull()?.message.toString()

fun <T> Result<T>.toUIState(): UiState<T> {
    return fold(
        onSuccess = {
            UiState.Content(it)
        }, onFailure = {
            UiState.Error(this.getFailureValue())
        }
    )
}
