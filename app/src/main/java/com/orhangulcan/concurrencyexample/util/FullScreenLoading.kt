package com.orhangulcan.concurrencyexample.util

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.tooling.preview.Preview

const val FULL_SCREEN_LOADING_TEST_TAG = "FULL_SCREEN_LOADING_TEST_TAG"

@Preview
@Composable
fun FullScreenLoading() {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.Center)
            .testTag(FULL_SCREEN_LOADING_TEST_TAG)
    ) {
        CircularProgressIndicator(color = MaterialTheme.colors.primary)
    }
}
