package com.orhangulcan.concurrencyexample.util

import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.NavGraph
import com.orhangulcan.concurrencyexample.R

fun NavDestination.hasBottomNav(navController: NavController): Boolean {
    val backStackCount = navController.backQueue.filter { it.destination !is NavGraph }.size
    return id == R.id.fragmentOne && backStackCount <= 2
}
